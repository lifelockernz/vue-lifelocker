import { adminRoot } from "./config";
import { UserRole } from "../utils/auth.roles";

const data = [{
  id: "dashboards",
  icon: "iconsminds-shop-4",
  label: "menu.dashboards",
  to: `${adminRoot}/dashboards`
  // roles: [UserRole.Admin, UserRole.Editor],
},
{
  id: "create-a-will",
  icon: "iconsminds-profile",
  label: "Create a Will",
  to: `${adminRoot}/create-a-will`
},
{
  id: "messages",
  icon: "iconsminds-camera-4",
  label: "Messages",
  to: `${adminRoot}/messages`
},
{
  id: "accounts",
  icon: "iconsminds-instagram",
  label: "Accounts",
  to: `${adminRoot}/accounts`
}
];
export default data;
