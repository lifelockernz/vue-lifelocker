export const pricesData = {
    en: [
        {
            icon: 'iconsminds-money-bag',
            title: 'FREE',
            price: '$0',
            detail: 'LifeTime',
            link: '#',
            features: [
                'Upload a Will'
            ]
        },
        {
            icon: 'simple-icon-energy',
            title: 'SILVER PACKAGE',
            price: '$4.95',
            detail: 'Per Month',
            link: '#',
            features: [
                'Upload a Will',
                'Personal Accounts Manager',
                'Personal Messaging',
                'Asset Management'
            ]
        },
        {
            icon: 'simple-icon-trophy',
            title: 'GOLD PACKAGE',
            price: '$9.95',
            detail: 'Per Month',
            link: '#',
            features: [
                'Upload a Will',
                'Personal Accounts Manager',
                'Personal Messaging',
                'Asset Management',
                'Legal support'
            ]
        }
    ],

}
