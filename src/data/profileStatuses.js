const data = [
  {
    title: 'Basic Information',
    total: 18,
    status: 12
  },
  {
    title: 'Your Will Progress',
    total: 8,
    status: 1
  },
  {
    title: 'Manage Personal Accounts',
    total: 6,
    status: 2
  },
  {
    title: 'Personal Messages',
    total: 10,
    status: 0
  },
  {
    title: 'Uploaded Will',
    total: 1,
    status: 1
  }
]
export default data
